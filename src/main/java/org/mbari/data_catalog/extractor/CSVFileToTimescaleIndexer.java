package org.mbari.data_catalog.extractor;

import com.google.gson.internal.LinkedTreeMap;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CSVFileToTimescaleIndexer {

    // A logger
    private static Logger logger = LogManager.getLogger(CSVFileToTimescaleIndexer.class);

    // This is the data source that is used to work with the database
    private ComboPooledDataSource comboPooledDataSource;

    // The DB connection
    private Connection dbConnection;

    // A regexp to help parse doubles.  It patterns matches on doubles at the start of a string
    private Pattern decimalAtStartPattern = Pattern.compile("^([+-]?\\d*(([,.]\\d{3})+)?([.]\\d+))\\D+");

    /**
     * The default constructor
     */
    public CSVFileToTimescaleIndexer(ComboPooledDataSource comboPooledDataSource) {
        this.comboPooledDataSource = comboPooledDataSource;
        try {
            this.dbConnection = this.comboPooledDataSource.getConnection();
        } catch (SQLException e) {
            logger.error("SQLException trying to get connection to the database: " + e.getMessage());
        }
    }

    /**
     * This is the method to index a file which should have time and maybe space variables and insert that data into
     * a table in a database.
     *
     * @param fileToIndex
     * @param uuid
     * @param dataTreeMap
     * @param numRecordsToSkip
     */
    public Long indexFile(File fileToIndex, String uuid, LinkedTreeMap dataTreeMap, Long numRecordsToSkip) {

        // Create an index to keep track of how many records we have processed so we can return it
        Long numOfRecordsProcessed = 0L;

        // First, let's make sure the file exists locally, if it doesn't there is no point continuing
        if (fileToIndex.exists()) {

            // Let's make sure the data map coming in is not null before we go any farther
            if (dataTreeMap != null) {

                // The columns we care about
                int timestampColumn = -1;
                int latitudeColumn = -1;
                int longitudeColumn = -1;
                int depthColumn = -1;
                int altitudeColumn = -1;

                // This is a HashMap that contains the column index and points to a varaiable name that will be needed for
                // parsing the data file
                HashMap<Integer, String[]> columnToVariableNameAndTypeMap = new HashMap<>();

                // OK, try to find the array list of variables
                if (dataTreeMap.get("variables") != null) {

                    // And try to cast it
                    ArrayList<LinkedTreeMap> variableArrayList = null;
                    try {
                        variableArrayList = (ArrayList<LinkedTreeMap>) dataTreeMap.get("variables");
                    } catch (Exception e) {
                        logger.error("Exception caught trying to cast the 'variables' property to an array list: " +
                                e.getMessage());
                    }

                    // If the array list is there, let's keep going
                    if (variableArrayList != null) {

                        // Iterate over the variables
                        for (LinkedTreeMap variableMap : variableArrayList) {

                            // We are looking for the following from each variable
                            String shortName = null;
                            String dataType = null;
                            String units = null;
                            Integer columnIndex = null;

                            // Grab the short name
                            shortName = (String) variableMap.get("short_name");
                            dataType = (String) variableMap.get("data_type");

                            // Now, the units and column index will be in the attributes array, so let's iterate over that
                            if (variableMap.containsKey("attributes")) {
                                // Grab the array list of attributes
                                ArrayList<LinkedTreeMap> attributeArrayList =
                                        (ArrayList<LinkedTreeMap>) variableMap.get("attributes");

                                // Now iterate on those
                                for (LinkedTreeMap attributeMap : attributeArrayList) {
                                    // If the short name is 'units' and the value is 'epoch seconds'
                                    if (attributeMap.containsKey("short_name") && attributeMap.get("short_name") != null) {
                                        // Check if this is the units attribute
                                        if (((String) attributeMap.get("short_name")).equals("units")) {
                                            units = (String) attributeMap.get("value");
                                        }

                                        // If it's the 'column_index', grab that and convert to an Integer
                                        if (((String) attributeMap.get("short_name")).equals("column_index")) {
                                            columnIndex = Integer.parseInt((String) attributeMap.get("value"));
                                        }
                                    }
                                }
                            }
                            logger.debug("Variable short name: " + shortName + ", units: " + units +
                                    ", columnIndex: " + columnIndex);

                            // If we have a column index and a variable name, add it to the map
                            if (columnIndex >= 0 && shortName != null) {
                                columnToVariableNameAndTypeMap.put(columnIndex, new String[]{shortName, dataType});
                            }

                            // Now we need to decide if it's one of the ones we care about.
                            if (timestampColumn < 0 && shortName != null &&
                                    (shortName.equalsIgnoreCase("timestamp") ||
                                            shortName.equalsIgnoreCase("datetime"))) {
                                timestampColumn = columnIndex;
                            }
                            if (timestampColumn < 0 && units != null &&
                                    (units.equalsIgnoreCase("epoch seconds"))) {
                                timestampColumn = columnIndex;
                            }
                            if (timestampColumn < 0 && units != null &&
                                    units.equalsIgnoreCase("epoch milliseconds")) {
                                timestampColumn = columnIndex;
                            }
                            if (latitudeColumn < 0 && shortName != null &&
                                    shortName.equalsIgnoreCase("latitude")) {
                                latitudeColumn = columnIndex;
                            }
                            if (longitudeColumn < 0 && shortName != null &&
                                    shortName.equalsIgnoreCase("longitude")) {
                                longitudeColumn = columnIndex;
                            }
                            if (depthColumn < 0 && shortName != null &&
                                    shortName.equalsIgnoreCase("depth")) {
                                depthColumn = columnIndex;
                            }
                            if (altitudeColumn < 0 && shortName != null &&
                                    shortName.equalsIgnoreCase("altitude")) {
                                altitudeColumn = columnIndex;
                            }
                        }
                    } else {
                        logger.warn("variableArrayList came back null after casting");
                    }
                } else {
                    logger.warn("No variables were found in the data tree map for file " + fileToIndex.getAbsolutePath());
                }

                // OK, I should now know if there are timestamp, latitude, longitude, depth or altitude columns. Since
                // timestamp is the minimum needed for parsing, let's make sure we have that
                if (timestampColumn >= 0) {
                    logger.debug("We have a timestamp column and it's column number " + timestampColumn);

                    // The first thing to do is make sure we have a table named the uuid and if not, create it
                    if (!this.doesTableExist(uuid)) {
                        logger.debug("Does not look like the table for UUID " + uuid +
                                " exists yet, will create it");
                        this.createTable(uuid, columnToVariableNameAndTypeMap);
                    }

                    // OK, table should be there and ready to go so double check it
                    if (this.doesTableExist(uuid)) {
                        logger.debug("Table is ready to go, so let's extract the data");

                        // Create the CSVFormat object
                        CSVFormat csvFormat = CSVFormat.RFC4180.withFirstRecordAsHeader();

                        // Create the file reader
                        Reader in = null;
                        try {
                            in = new FileReader(fileToIndex);
                        } catch (FileNotFoundException e) {
                            logger.error("FileNotFoundException looking for " + fileToIndex);
                        }

                        // If the reader is OK
                        if (in != null) {

                            // Now parse the file into records
                            CSVParser parser = null;
                            try {
                                parser = csvFormat.parse(in);
                            } catch (IOException e) {
                                logger.error("IOException caught trying to create CSV parser for " + fileToIndex);
                                logger.error("Message: " + e.getMessage());
                            }

                            // If the parse seems OK
                            if (parser != null) {

                                // This is the number of statements that will be batched
                                int batchSizeLimit = 1000;

                                // This is the number of inserts that have been added to the statement
                                int numInsertsAdded = 0;

                                // This is the Statement that will be used in the loop to batch insert data
                                Statement batchInsertStatement = null;

                                // Create an array to keep track of timestamp already processed
                                ArrayList<Long> processedTimestamps = new ArrayList<>();

                                // Now loop over the records
                                for (CSVRecord record : parser) {

                                    // Bump the counter
                                    numOfRecordsProcessed++;

                                    // Let's start with the timestamp column
                                    String timestampString = record.get(timestampColumn);

                                    // Now this string could be epoch millis, epoch seconds or a date time string.
                                    // Call the method to try and parse a epoch seconds number out of it.
                                    Long epochMillis = this.getEpochMillisecondsFromString(timestampString);

                                    // Make sure it's not one we have parsed already
                                    if (numOfRecordsProcessed > numRecordsToSkip) {

                                        // Now check to see if the timestamp was already processed
                                        if (!processedTimestamps.contains(epochMillis)) {
                                            // Let's first make sure the number of columns in the record match the number of
                                            // variables, if not, log warning and skip.
                                            if (record.size() == columnToVariableNameAndTypeMap.keySet().size()) {

                                                // Make sure that converted OK, of the deal is off
                                                if (epochMillis != null) {
                                                    // Create a builder that will contain the column names for the insert
                                                    // statement
                                                    StringBuilder variableBuilder = new StringBuilder();

                                                    // Can add the timestamp and location as they will be there, even if location
                                                    // is null
                                                    variableBuilder.append("(data_catalog_timestamp, data_catalog_location");

                                                    // Create a builder that will contain the values for the insert statement
                                                    StringBuilder valueBuilder = new StringBuilder();

                                                    // Add the timestamp
                                                    valueBuilder.append("(TO_TIMESTAMP(" + epochMillis + "::double precision/1000)");

                                                    // Now do we have latitude and longitude
                                                    if (latitudeColumn >= 0 && longitudeColumn >= 0) {
                                                        // Grab the latitude value from the record
                                                        Double latitude = parseDoubleFromString(record.get(latitudeColumn));
                                                        Double longitude = parseDoubleFromString(record.get(longitudeColumn));
                                                        if (latitude != null && longitude != null) {
                                                            valueBuilder.append(", ST_SetSRID(ST_MakePoint(" +
                                                                    longitude + ", " + latitude + "), 4326)");
                                                        } else {
                                                            valueBuilder.append(", null");
                                                        }
                                                    } else {
                                                        valueBuilder.append(", null");
                                                    }

                                                    // Now that timestamp and location are added, let's do the rest of the
                                                    // variables
                                                    for (Integer columnIndex : columnToVariableNameAndTypeMap.keySet()) {

                                                        // Add the column name to the variable builder
                                                        variableBuilder.append(", " +
                                                                this.convertVariableNameToColumnName(
                                                                        columnToVariableNameAndTypeMap.get(columnIndex)[0]));

                                                        // Grab the value
                                                        String columnValue = record.get(columnIndex);

                                                        // Now grab the data type for conversion
                                                        if (columnToVariableNameAndTypeMap.get(columnIndex)[1].equals("bigint")) {
                                                            // Try to convert the value to long
                                                            Long columnValueLong = null;
                                                            try {
                                                                columnValueLong = Long.parseLong(columnValue);
                                                            } catch (NumberFormatException e) {
                                                                logger.warn("NumberFormatException caught trying to parse " +
                                                                        "column value of " + columnValue +
                                                                        " into a long as the data type should be bigint");
                                                            }
                                                            // If it converted OK, add it to the valueBuilder
                                                            if (columnValueLong != null) {
                                                                valueBuilder.append(", " + columnValueLong);
                                                            } else {
                                                                valueBuilder.append(", null");
                                                            }
                                                        } else {
                                                            // Not a bigint, so let's look for a float
                                                            if (columnToVariableNameAndTypeMap.get(columnIndex)[1]
                                                                    .equals("float8")) {
                                                                // Try to convert to Double
                                                                Double columnValueDouble = parseDoubleFromString(columnValue);

                                                                // If it converted OK, add it, otherwise, set it to null
                                                                if (columnValueDouble != null) {
                                                                    valueBuilder.append(", " + columnValueDouble);
                                                                } else {
                                                                    valueBuilder.append(", null");
                                                                }
                                                            } else {
                                                                // Check for timestamp
                                                                if (columnToVariableNameAndTypeMap.get(columnIndex)[1]
                                                                        .equals("timestamp")) {
                                                                    // Try to convert the value to epoch millis
                                                                    Long columnValueEpochMillis = null;
                                                                    columnValueEpochMillis =
                                                                            this.getEpochMillisecondsFromString(columnValue);
                                                                    if (columnValueEpochMillis != null) {
                                                                        valueBuilder.append(", TO_TIMESTAMP(" +
                                                                                columnValueEpochMillis + "/1000)");
                                                                    } else {
                                                                        // Insert null
                                                                        valueBuilder.append(", null");
                                                                    }
                                                                } else {
                                                                    // Check for boolean
                                                                    if (columnToVariableNameAndTypeMap.get(columnIndex)[1]
                                                                            .equals("boolean")) {
                                                                        if (columnValue.toLowerCase().equals("true")) {
                                                                            valueBuilder.append(", true");
                                                                        } else if (columnValue.toLowerCase().equals("false")) {
                                                                            valueBuilder.append(", false");
                                                                        } else {
                                                                            valueBuilder.append(", null");
                                                                        }
                                                                    } else {
                                                                        // Make sure it's text
                                                                        if (columnToVariableNameAndTypeMap.get(columnIndex)[1]
                                                                                .equals("text")) {
                                                                            valueBuilder.append(", '" + columnValue + "'");
                                                                        } else {
                                                                            // Just insert null, don't know what's up
                                                                            logger.warn("Did not recognize the data type of " +
                                                                                    columnToVariableNameAndTypeMap.get(columnIndex)[1] +
                                                                                    " so just inserted null");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    // Done with columns, close the builders
                                                    variableBuilder.append(")");
                                                    valueBuilder.append(")");

                                                    // Create the entire statement
                                                    String insertStatementString = "INSERT INTO \"" + uuid + "\" " +
                                                            variableBuilder.toString() + " VALUES " + valueBuilder.toString();

                                                    // Make sure we have a SQL statement to add inserts to
                                                    if (batchInsertStatement == null) {
                                                        try {
                                                            batchInsertStatement =
                                                                    this.dbConnection.createStatement();
                                                            logDBConnectionStats("After creating batch insert statement");
                                                        } catch (SQLException e) {
                                                            logger.error("SQLException caught trying to create batch " +
                                                                    "insert statement: " + e.getMessage());
                                                        }
                                                    }

                                                    // Add it to the batch
                                                    if (batchInsertStatement != null) {
                                                        try {
                                                            batchInsertStatement.addBatch(insertStatementString);
                                                            numInsertsAdded++;
                                                        } catch (SQLException e) {
                                                            logger.error("SQLException caught trying to add statement " +
                                                                    "to batch: " + e.getMessage());
                                                            logger.error(insertStatementString);
                                                        }

                                                        // If the number of statements has reached it's limit, do the batch
                                                        if (numInsertsAdded >= batchSizeLimit) {
                                                            try {
                                                                batchInsertStatement.executeBatch();
                                                                logger.debug("Batch inserted " + numInsertsAdded +
                                                                        " records");
                                                            } catch (SQLException e) {
                                                                logger.error("SQLException caught trying to execute batch: " + e.getMessage());
                                                            }
                                                            // Close the batch insert statement
                                                            if (batchInsertStatement != null) {
                                                                try {
                                                                    batchInsertStatement.close();
                                                                } catch (SQLException e) {
                                                                    logger.error("SQLException caught trying to close " +
                                                                            "batch after execution: " + e.getMessage());
                                                                }
                                                                batchInsertStatement = null;
                                                            }
                                                            logDBConnectionStats("After batch execute and batch close");
                                                            // Reset the counter
                                                            numInsertsAdded = 0;
                                                        }
                                                    }
                                                } else {
                                                    logger.warn("Could not convert the value from the timestamp column " +
                                                            timestampString +
                                                            " to epoch millis, nothing more will be done today.");
                                                }
                                            } else {
                                                logger.warn("Record " + numOfRecordsProcessed +
                                                        " in the file does not have the same number of columns as the " +
                                                        "header, it will be skipped");
                                            }
                                        } else {
                                            logger.warn("Looks like the record has a timestamp that " +
                                                    "was already processed");
                                            logger.warn("Record: " + record.toString());
                                        }
                                    }

                                    // Done processing the record, add the timestamp to the array of processed timestamps
                                    if (!processedTimestamps.contains(epochMillis))
                                        processedTimestamps.add(epochMillis);
                                }

                                // We are done looping over the records, but it's likely that we have some inserts
                                // that were added to the batch, but not executed
                                if (numInsertsAdded > 0 && batchInsertStatement != null) {
                                    try {
                                        batchInsertStatement.executeBatch();
                                        logger.debug("Batch inserted " + numInsertsAdded +
                                                " records");
                                    } catch (SQLException e) {
                                        logger.error("SQLException caught trying to run last batch execute: " +
                                                e.getMessage());
                                    }
                                    if (batchInsertStatement != null) {
                                        try {
                                            batchInsertStatement.close();
                                        } catch (SQLException e) {
                                            logger.error("SQLException caught after last batch statement close: " + e.getMessage());
                                        }
                                    }
                                    logDBConnectionStats("After final execute and close of batch");
                                }
                            } else {
                                logger.warn("The CSV parser came back null for file " +
                                        fileToIndex.getAbsolutePath());
                            }
                        } else {
                            logger.warn("Something went wrong trying to open a file reader to " +
                                    fileToIndex.getAbsolutePath() + " so no parsing done.");
                        }
                    } else {
                        logger.error("Could not seem to create a table with UUID " + uuid +
                                ", cannot index the file unless we have it");
                    }
                } else {
                    logger.warn("No timestamp column was found in the file " + fileToIndex.getAbsolutePath() +
                            " so no data was extracted from the file");
                }
            } else {
                logger.warn("There was no data tree map so I can't parse the CSV file");
            }
        } else {
            logger.warn("The method to index a CSV file located at " + fileToIndex.getAbsolutePath() +
                    " was called, but the file does not exist on the server where this code is " +
                    "running so nothing was done");
        }

        // Return the number of records processed
        return numOfRecordsProcessed;
    }

    /**
     * This method runs a check to see if there is a table named uuid already in the database
     *
     * @param uuid
     * @return
     */
    private boolean doesTableExist(String uuid) {

        // A flag indicating the table already exists
        boolean tableExists = false;

        // Make sure there is a table name to query for
        if (uuid != null && !uuid.equals("")) {
            // Grab the metadata from the database
            DatabaseMetaData meta = null;
            try {
                meta = this.dbConnection.getMetaData();
            } catch (SQLException e) {
                logger.error("SQLException trying to read metadata from database: " + e.getMessage());
            }

            // If it's found, keep going
            if (meta != null) {

                // Create a placeholder for the query results
                ResultSet resultSet = null;
                try {
                    // Run the query looking for the table
                    resultSet = meta.getTables(null, null, uuid, new String[]{"TABLE"});
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to find a table named " + uuid + ": " + e.getMessage());
                }

                // Make sure we have a result
                if (resultSet != null) {
                    try {
                        // See if there is an entry (there should only be one)
                        if (resultSet.next()) {
                            // If we are here, then the table exists
                            tableExists = true;
                        }
                    } catch (SQLException e) {
                        logger.error("SQLException looping over results that were returned from the query " +
                                "to find the tracking table");
                    }
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        logger.error("SQLException trying to close result set after search for table named " +
                                uuid + ": " + e.getMessage());
                    }
                } else {
                    logger.warn("ResultSet came back null from query for table " + uuid);
                }
            } else {
                logger.warn("While checking to see if the table " + uuid +
                        " already exists, the Metadata from database came back null");
            }
            logDBConnectionStats("After checking to see if table " + uuid + " exists");
        }

        // Return the result
        return tableExists;
    }

    /**
     * @param uuid
     * @param columnToVariableNameAndTypeMap
     */
    private void createTable(String uuid, HashMap<Integer, String[]> columnToVariableNameAndTypeMap) {

        logger.debug("createTable called for uuid " + uuid);

        // The table structures all start the same with a timestamp column and a location (geometry) column
        StringBuilder createTableBuilder = new StringBuilder();

        createTableBuilder.append("CREATE TABLE \"" + uuid + "\" (");
        createTableBuilder.append("data_catalog_timestamp timestamp(3) PRIMARY KEY,");
        createTableBuilder.append("data_catalog_location geometry");

        // Make sure the incoming hash map is there
        if (columnToVariableNameAndTypeMap != null) {
            // Loop over the keys which are actually the column indices
            for (Integer columnIndex : columnToVariableNameAndTypeMap.keySet()) {
                // Grab the data type first
                String dataType = columnToVariableNameAndTypeMap.get(columnIndex)[1];

                // If it's null, just make it text
                if (dataType == null) dataType = "text";

                // Now make sure it's one of what we expect
                if (dataType.equals("text") || dataType.equals("boolean") || dataType.equals("timestamp") ||
                        dataType.equals("bigint") || dataType.equals("float8")) {
                    // Grab the name and add it to the list
                    createTableBuilder.append(", " +
                            this.convertVariableNameToColumnName(columnToVariableNameAndTypeMap.get(columnIndex)[0]) +
                            " " + dataType);
                } else {
                    // Grab the name and add it to the list
                    createTableBuilder.append(", " +
                            this.convertVariableNameToColumnName(columnToVariableNameAndTypeMap.get(columnIndex)[0]) +
                            " text");
                }
            }
        } else {
            logger.warn("While trying to create the table, columnToVariableNameAndTypeMap was null");
        }

        // Finish the create statement
        createTableBuilder.append(")");
        logger.debug("Create table statement: " + createTableBuilder.toString());

        // Go ahead and create the table
        Statement createTableStatement = null;
        try {
            createTableStatement = this.dbConnection.createStatement();
        } catch (SQLException e) {
            logger.error("SQLException caught trying to create statement to create table for " + uuid + ": " +
                    e.getMessage());
        }
        if (createTableStatement != null) {
            try {
                createTableStatement.executeUpdate(createTableBuilder.toString());
            } catch (SQLException e) {
                logger.error("SQLException caught trying to create a table for UUID " + uuid + ": " + e.getMessage());
            }
        }
        if (createTableStatement != null) {
            try {
                createTableStatement.close();
            } catch (SQLException e) {
                logger.error("SQLException caught trying to close create table statement for table " +
                        uuid + ": " + e.getMessage());
            }
        }
        logDBConnectionStats("After creation of table " + uuid);
    }

    /**
     * This method takes in a String that is variable name and then converts it to something that can be used for a
     * column name in a database.  Some variable names have spaces and special characters which do not work as names.
     * This method basically replaces any non-usable characters with underscores.
     *
     * @param variableName
     * @return
     */
    private String convertVariableNameToColumnName(String variableName) {

        // The string to return
        String columnName = null;

        // If there is a variable name
        if (variableName != null) {
            // Replace any non alpha-numerics with underscores
            columnName = variableName.replaceAll("[^A-Za-z0-9]", "_");

            // Now replace duplicate underscores with just one
            columnName = columnName.replaceAll("_{2,}", "_");

            // Remove any starting or ending underscores
            columnName = columnName.replaceAll("_$", "");
            columnName = columnName.replaceAll("^_", "");

            // Now all should be lower case
            columnName = columnName.toLowerCase();
        }

        // Return the name
        return columnName;
    }

    /**
     * This method takes in a timestamp and tries it's best to convert it to a date and then return the epoch seconds
     * from that date.  It first tries to parse the string into a Long and then, if successful, converts to a Date to
     * perform a sanity check on the date. If it looks OK, it then returns the epoch seconds of that date.  If it cannot
     * convert it from a Long, it tries to parse the string using JODA DateTime (which is expecting ISO formatted date)
     * and then grabs the epoch seconds from that.  If none of that works, it returns null.
     *
     * @param timestamp
     * @return
     */
    private Long getEpochMillisecondsFromString(String timestamp) {
        // The long to return
        Long timestampToReturn = null;

        // First let's try to convert the String to a long
        try {
            timestampToReturn = Long.parseLong(timestamp);
        } catch (NumberFormatException e) {
        }

        // If that was successful, we need to run a sanity check
        if (timestampToReturn != null) {
            // The first thing to do for the sanity check is to multiply the number by 1000 to convert to epoch millis
            // and then create a date from that.  If the date is way in the future, we will make the assumption that the
            // timestamp is already in epoch millis

            // Convert to what could be millis
            Long timestampConvertedToMillis = timestampToReturn * 1000;

            // If the date is past the year 3000, assume the original number was already in millis
            if (timestampConvertedToMillis > 32503680000000L) {
            } else {
                timestampToReturn = timestampConvertedToMillis;
            }
        } else {
            // This could be a ISO formatted string, let's try that
            DateTime dateTime = null;
            try {
                dateTime = new DateTime(timestamp);
            } catch (Exception e) {
            }
            // If it looks like it converted OK, return the millis from it
            if (dateTime != null) timestampToReturn = dateTime.toDate().getTime();
        }

        if (timestampToReturn == null) {
            logger.debug("Could not seem to convert " + timestamp + " string into epoch millis");
        }

        // Return it
        return timestampToReturn;
    }

    /**
     * @param doubleString
     * @return
     */
    private Double parseDoubleFromString(String doubleString) {
        // The double to return
        Double doubleToReturn = null;

        // Try to do a straight up parsing
        try {
            doubleToReturn = Double.parseDouble(doubleString);
        } catch (NumberFormatException e) {
            logger.debug("NumberFormatException trying to parse " + doubleString +
                    " into a double, will try to find double at beginning of string");
        }

        // If the double is still null, see if we can get a double from the start of the string and ignore everything else
        if (doubleToReturn == null) {
            Matcher doubleMatcher = decimalAtStartPattern.matcher(doubleString.trim());
            if (doubleMatcher.matches()) {
                // Try to parse what should be the double part
                logger.debug("Since double parsing failed and it looks like there is a double at the start " +
                        "of the string, will try to parse this into a double: " + doubleMatcher.group(1));
                try {
                    doubleToReturn = Double.parseDouble(doubleMatcher.group(1));
                } catch (NumberFormatException e) {
                    logger.debug("NumberFormatException caught trying to parse for double at the " +
                            "beginning of the string so I give up");
                }
            }
        }
        // Return the result
        return doubleToReturn;
    }

    /**
     * @param context
     */
    private void logDBConnectionStats(String context) {
//        try {
//            logger.debug("Context: " + context);
//            logger.debug("DB: NumConnections: " + this.comboPooledDataSource.getNumConnections());
//            logger.debug("DB: NumBusyConnections: " + this.comboPooledDataSource.getNumBusyConnections());
//        } catch (SQLException e) {
//            logger.error("SQLException caught trying to log DB connection stats: " + e.getMessage());
//        }
    }
}
