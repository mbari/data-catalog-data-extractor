package org.mbari.data_catalog.extractor;

import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mbari.data_catalog.ApiClient;
import org.mbari.data_catalog.ApiException;
import org.mbari.data_catalog.api.DefaultApi;
import org.mbari.data_catalog.model.Response;
import org.mbari.data_catalog.model.ResponseData;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * This class has a main method and is responsible for searching a data catalog API and looking for any resources that
 * have files that are accessible by this application.  If a resource is associated with a file that is available
 * locally and is a file type that this application is looking for, it will process it in different ways.  It can
 * extract time/space data into a timescale database, register the file with a Geoserver instance, etc.
 */
public class APICrawler extends Thread {

    // A logger
    private static Logger logger = LogManager.getLogger(APICrawler.class);

    // The list of repositories to crawl
    private String repoName = null;

    // The base of the file URI from the catalog
    private String filePathBase = null;

    // The same location, but where it is located on the local machine
    private String filePathBaseLocal = null;

    // This is the number of minutes to pause between cycles of crawling
    private Integer numMinutesToPause = 5;

    // The API base path
    private String apiBasePath;

    // Flag to indicate if we should verify SSL on the API
    private boolean verifySSL;

    // This is the API interface
    private DefaultApi apiInstance;

    // These are the properties for the Timescale database indexer
    private String databaseHost;
    private Integer databasePort;
    private String databaseName;
    private String databaseUsername;
    private String databasePassword;

    // This is the instance of the class that will index files into Timescale database
    private FileToTimescaleIndexer fileToTimescaleIndexer;

    // These are the properties for the Geoserver indexer
    private String geoserverBaseUrl;
    private String geoserverUsername;
    private String geoserverPassword;
    private String geoserverFilePathBase;

    // The geoserver indexer
    private GeoserverIndexer geoserverIndexer;

    /**
     * This is the constructor that takes in the API base path and whether or not it's need to verify the SSL
     * certificate
     *
     * @param apiBasePath
     * @param verifySSL
     */
    public APICrawler(String repoName, String filePathBase, String filePathBaseLocal, Integer numMinutesToPause,
                      String apiBasePath, Boolean verifySSL, String databaseHost, Integer databasePort,
                      String databaseName, String databaseUsername, String databasePassword, String geoserverBaseUrl,
                      String geoserverUsername, String geoserverPassword, String geoserverFilePathBase) {

        // If name were specified
        this.repoName = repoName;
        this.filePathBase = filePathBase;
        this.filePathBaseLocal = filePathBaseLocal;

        // If number of minutes was specified, set it
        this.numMinutesToPause = numMinutesToPause;

        // The API properties
        this.apiBasePath = apiBasePath;
        this.verifySSL = verifySSL;

        // The database connection properties
        this.databaseHost = databaseHost;
        this.databasePort = databasePort;
        this.databaseName = databaseName;
        this.databaseUsername = databaseUsername;
        this.databasePassword = databasePassword;

        // The geoserver properties
        this.geoserverBaseUrl = geoserverBaseUrl;
        this.geoserverUsername = geoserverUsername;
        this.geoserverPassword = geoserverPassword;
        this.geoserverFilePathBase = geoserverFilePathBase;

        // Create an API client if there are properties specified
        if (this.apiBasePath != null) {
            // Create the client
            ApiClient apiClient = new ApiClient();

            // Set the base path
            apiClient.setBasePath(apiBasePath);

            // Set whether or not it needs to verify the SSL certificate
            apiClient.setVerifyingSsl(verifySSL);

            // Now create the API client instance
            apiInstance = new DefaultApi(apiClient);
        }

        // Check for DB properties and if they are there, go ahead and create the database indexer
        if (this.databaseHost != null) {
            fileToTimescaleIndexer = new FileToTimescaleIndexer(databaseHost, databasePort, databaseName,
                    databaseUsername, databasePassword);
        }

        // If the geoserver properties are there, create a Geoserver indexer
        if (this.geoserverBaseUrl != null) {
            this.geoserverIndexer = new GeoserverIndexer(this.geoserverBaseUrl, this.geoserverUsername,
                    this.geoserverPassword, this.filePathBaseLocal, this.geoserverFilePathBase);
        }
    }

    /**
     * This method starts the crawling of the API to looks for Resources that can be indexed
     */
    public void run() {

        // Start infinite loop of crawling
        while (true) {

            // The number of items to request
            Integer itemsPerPage = 25;

            // This is a flag to indicate if all the records have been dealt with
            Boolean done = false;

            // The starting index
            Integer startIndex = 0;

            // Now loop until done flag is cleared (for looping through paged results)
            while (!done) {

                // Query the API for any Resources that have data fields associated with them
                Response result = null;
                try {
                    result = this.apiInstance.getResources(repoName, null, false, null,
                            null, null, null, null,
                            null, null, null, null,
                            null, null, startIndex, itemsPerPage,
                            "file,path,data,metadata");
                } catch (ApiException e) {
                    logger.error("ApiException caught trying to find any resources with data: " + e.getMessage());
                }

                // If a result was found
                if (result != null) {
                    // The result is actually a Response object and the returned resources are in the data array.
                    // Before we do anything else, let's check the size of the return and if it's less than the
                    // number of items per page requested, we know this is the last page of the request.  If not,
                    // bump the startIndex by the itemsPerPage so the next request will grab the next chunk.
                    if (result.getData() != null) {

                        // Grab the response data
                        ResponseData responseData = result.getData();

                        // Grab the items associated with the response data
                        Object items = responseData.getItems();

                        // Check to see if the items are an array list (they should be)
                        if (items instanceof ArrayList) {

                            // Cast it to an array list
                            ArrayList responseArrayList = (ArrayList) items;

                            // Now loop over responses
                            for (Object response : responseArrayList) {
                                // Bump the start index
                                startIndex++;

                                // The response object should be a TreeMap
                                if (response instanceof LinkedTreeMap) {

                                    // Cast it
                                    LinkedTreeMap responseTreeMap = (LinkedTreeMap) response;

                                    // Index it
                                    this.indexResponse(responseTreeMap);

                                } else {
                                    logger.warn("Not a TreeMap");
                                }
                            }
                        } else {
                            done = true;
                        }
                    } else {
                        // The response data was null, we should just set the done flag
                        logger.warn("Query result was not null, but the ResponseData object was null.");
                        logger.warn("With no ResponseData object, we will exit the crawling loop for repo " + repoName);
                        done = true;
                    }
                } else {
                    // This should not happen, but log that and set the done flag
                    logger.warn("The result coming back from the query was null, should not happen, " +
                            "but could just mean no results were found");
                    done = true;
                }
            }

            // Sleep for a bit before going through it all again
            try {
                logger.debug("Done crawling the API resources, will sleep for " + this.numMinutesToPause +
                        " minutes before starting another cycle");
                Thread.sleep(this.numMinutesToPause * 60 * 1000);
            } catch (InterruptedException e) {
                logger.error("InterruptedException caught trying to sleep the thread for " + this.numMinutesToPause +
                        " minutes");
            }
        }
    }

    /**
     * @param responseTreeMap
     */
    private void indexResponse(LinkedTreeMap responseTreeMap) {
        // Grab the UUID of the document
        String uuid = (String) responseTreeMap.get("uuid");

        // Make sure the UUID was found
        if (uuid != null) {

            // Grab the file information
            LinkedTreeMap fileTreeMap = (LinkedTreeMap) responseTreeMap.get("file");

            // Grab the path object, which is another tree map
            LinkedTreeMap pathTreeMap = (LinkedTreeMap) responseTreeMap.get("path");

            // Grab the data description object
            LinkedTreeMap dataTreeMap = (LinkedTreeMap) responseTreeMap.get("data");

            // Grab the metadata object
            LinkedTreeMap metadataTreeMap = (LinkedTreeMap) responseTreeMap.get("metadata");

            // Now grab the file real path that is in the catalog
            String realPath = (String) pathTreeMap.get("real");

            // Now replace the base path from the catalog with that of what should be locally
            String fileUriLocal = realPath.replace(filePathBase, filePathBaseLocal);

            // Now build the file pointer
            File localDataFile = new File(fileUriLocal);

            // If the file exists locally, has data properties and there is a fileindexer available, go ahead and
            // send the file to the indexer
            if (localDataFile.exists() && dataTreeMap != null && this.fileToTimescaleIndexer != null) {

                logger.debug("File " + localDataFile.getAbsolutePath() +
                        " exists locally, and it looks like it contains data so will send it to the file indexer");
                fileToTimescaleIndexer.indexFile(localDataFile, uuid, dataTreeMap);
            }

            // If there is a geoserver indexer, let's see if the file might be something that needs to be indexed
            if (this.geoserverIndexer != null && fileTreeMap != null) {
                // Let's check for a MIME type
                if (fileTreeMap.containsKey("content_type") &&
                        fileTreeMap.get("content_type") != null) {

                    // Grab the content type to make code cleaner
                    String contentType = fileTreeMap.get("content_type").toString();

                    // Check to see if it's a TIFF file (could be a GeoTIFF)
                    if (contentType.equals("image/tiff") ||
                            contentType.equals("application/x-shapefile")) {
                        this.geoserverIndexer.indexFile(this.repoName, localDataFile, fileTreeMap);
                    }
                    // Check to see if it's a geopackage
                    if (fileTreeMap.get("content_type").equals("application/x-sqlite3") &&
                            fileTreeMap.containsKey("extension") &&
                            fileTreeMap.get("extension") != null &&
                            fileTreeMap.get("extension").equals("gpkg")) {
                        this.geoserverIndexer.indexFile(this.repoName, localDataFile, fileTreeMap);
                    }
                } else {
                    logger.debug("No content type for " + localDataFile.getAbsolutePath());
                }
            }
        } else {
            logger.warn("In indexResponse, looking for UUID returned nothing, so we can't do anything.");
        }
    }

    /**
     * The main method
     *
     * @param args
     */
    public static void main(String[] args) {

        // Try to read the necessary environment variables
        logger.info("Starting the API Crawler ...");

        // ********************************************************
        // API CRAWLER PROPERTIES
        // ********************************************************
        // Grab the DE_REPO_LIST environment variable
        String repoList = System.getenv("DE_REPO_LIST");

        // The variable should actually be a stringified version of a JSONArray of repository information so try to
        // convert the environment variable string to a JsonArray
        Gson gson = new Gson();
        JsonArray repoArray = null;
        try {
            repoArray = gson.fromJson(repoList, JsonArray.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        // Read in the number of minutes to pause in between API crawls
        Integer numMinutesToPause = 5;
        if (System.getenv("DE_NUM_MINUTES_TO_PAUSE") != null) {
            numMinutesToPause = Integer.parseInt(System.getenv("DE_NUM_MINUTES_TO_PAUSE"));
        }
        logger.info("Number of minutes between crawls set to " + numMinutesToPause);

        // ********************************************************
        // DATA CATLOG API CONNECTION PROPERTIES
        // ********************************************************
        String apiBasePath = null;
        if (System.getenv("DE_API_BASE_PATH") != null) {
            apiBasePath = System.getenv("DE_API_BASE_PATH");
        } else {
            logger.error("The API Base path (DE_API_BASE_PATH) was not found in the environment " +
                    "variables. Please fix");
            throw new IllegalArgumentException("The API Base path (DE_API_BASE_PATH) was not found in the " +
                    "environment variables. Please fix");
        }
        logger.info("API base path set to: " + apiBasePath);

        // Create a default verification of SSL certificate and then try to read one from the environment
        Boolean verifySSL = false;
        if (System.getenv("DE_API_VERIFY_SSL") != null) {
            verifySSL = Boolean.parseBoolean(System.getenv("DE_API_VERIFY_SSL"));
        }
        logger.info("Verify SSL certificate on API set to " + verifySSL);

        // ********************************************************
        // DATABASE CONNECTION PROPERTIES
        // ********************************************************
        String databaseHost = System.getenv("DE_DB_HOST");
        String databasePortString = System.getenv("DE_DB_PORT");
        Integer databasePort = null;
        try {
            databasePort = Integer.parseInt(databasePortString);
        } catch (NumberFormatException e) {
            logger.error("NumberFormatException caught trying to convert DB port " +
                    "environment variable (" + databasePortString + ") to an Integer: " + e.getMessage());
        }
        String databaseName = System.getenv("DE_DB_NAME");
        String databaseUsername = System.getenv("DE_DB_USERNAME");
        String databasePassword = System.getenv("DE_DB_PASSWORD");

        // TODO kgomes, validate the DB connection parameters

        // ********************************************************
        // GEOSERVER CONNECTION PROPERTIES
        // ********************************************************
        String geoserverBaseUrl = System.getenv("DE_GEOSERVER_BASE_URL");
        String geoserverUsername = System.getenv("DE_GEOSERVER_USERNAME");
        String geoserverPassword = System.getenv("DE_GEOSERVER_PASSWORD");

        // ********************************************************
        // Start up API Crawler(s)
        // ********************************************************
        if (repoArray != null && repoArray.size() > 0) {

            // Loop over the array or JSON objects which define the repositories to crawl
            for (int i = 0; i < repoArray.size(); i++) {

                // Grab the object
                JsonObject repoObject = (JsonObject) repoArray.get(i);

                // Make sure the object is there
                if (repoObject != null) {

                    // Grab the properties we need
                    String repoName = null;
                    String filePathBase = null;
                    String filePathBaseLocal = null;
                    String geoserverFilePathBase = null;
                    try {
                        repoName = repoObject.get("repoName").getAsString();
                    } catch (Exception e) {
                        logger.warn("Exception caught trying to read repoName from repo environment config: " +
                                e.getMessage());
                    }
                    try {
                        filePathBase = repoObject.get("filePathBase").getAsString();
                    } catch (Exception e) {
                        logger.warn("Exception caught trying to read filePathBase from repo environment config: " +
                                e.getMessage());
                    }
                    try {
                        filePathBaseLocal = repoObject.get("filePathBaseLocal").getAsString();
                    } catch (Exception e) {
                        logger.warn("Exception caught trying to read filePathBaseLocal from repo " +
                                "environment config: " + e.getMessage());
                    }
                    try {
                        geoserverFilePathBase = repoObject.get("geoserverFilePathBase").getAsString();
                    } catch (Exception e) {
                        logger.warn("Exception caught trying to read repogeoserverFilePathBaseName from repo " +
                                "environment config: " + e.getMessage());
                    }

                    // Make sure they all exist
                    if (repoName != null && !repoName.equals("") && filePathBase != null && !filePathBase.equals("") &&
                            filePathBaseLocal != null && !filePathBaseLocal.equals("")) {

                        // Now create the API Crawler
                        APICrawler apiCrawler = new APICrawler(repoName, filePathBase, filePathBaseLocal,
                                numMinutesToPause, apiBasePath, verifySSL, databaseHost, databasePort, databaseName,
                                databaseUsername, databasePassword, geoserverBaseUrl, geoserverUsername,
                                geoserverPassword, geoserverFilePathBase);

                        // Start the crawler
                        logger.info("Starting crawler loop");
                        apiCrawler.start();
                    }
                }
            }
        }
    }
}
