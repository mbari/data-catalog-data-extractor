package org.mbari.data_catalog.extractor;

import com.google.gson.internal.LinkedTreeMap;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mbari.data_catalog.JSON;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * This class is used a a tool to index files that have geospatial content into a Geoserver.
 */
public class GeoserverIndexer {

    // A logger
    private static Logger logger = LogManager.getLogger(FileToTimescaleIndexer.class);

    private String geoserverBaseUrl;
    private String geoserverUsername;
    private String geoserverPassword;
    private String filePathBaseLocal;
    private String geoserverFilePathBase;

    public GeoserverIndexer(String geoserverBaseUrl, String geoserverUsername, String geoserverPassword,
                            String filePathBaseLocal, String geoserverFilePathBase) {

        // Assign properties
        this.geoserverBaseUrl = geoserverBaseUrl;
        this.geoserverUsername = geoserverUsername;
        this.geoserverPassword = geoserverPassword;
        this.filePathBaseLocal = filePathBaseLocal;
        this.geoserverFilePathBase = geoserverFilePathBase;

        // Set up so SSL certificates are ignored
        try {
            SSLContext sslcontext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            CloseableHttpClient httpclient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .build();
            Unirest.setHttpClient(httpclient);
        } catch (NoSuchAlgorithmException e) {
            logger.error("NoSuchAlgorithmException caught trying to set up HTTP client to ignore SSL certificates: " + e.getMessage());
        } catch (KeyManagementException e) {
            logger.error("KeyManagementException caught trying to set up HTTP client to ignore SSL certificates: " + e.getMessage());
        } catch (KeyStoreException e) {
            logger.error("KeyStoreException caught trying to set up HTTP client to ignore SSL certificates: " + e.getMessage());
        }
    }

    public void indexFile(String repoName, File geospatialFileToIndex, LinkedTreeMap fileTreeMap) {
        logger.debug("Going to try to index file " + geospatialFileToIndex.getAbsolutePath());

        // Let's make sure there is an equivalent workspace name for the repo
        if (!doesWorkspaceExist(repoName)) {
            createWorkspace(repoName);
        }

        // Now let's see what kind of file we are dealing with
        if (fileTreeMap.containsKey("content_type") &&
                fileTreeMap.get("content_type") != null) {

            // Grab the content type to make code cleaner
            String contentType = fileTreeMap.get("content_type").toString();

            // Grab the file extension
            String fileExtension = null;
            if (fileTreeMap.containsKey("extension") &&
                    fileTreeMap.get("extension") != null) {
                fileExtension = fileTreeMap.get("extension").toString();
            }

            // First we need to figure out where the file would be located on the geoserver
            String absolutePathLocal = geospatialFileToIndex.getAbsolutePath();

            // Grab just the file name
            String filename =
                    geospatialFileToIndex.getName().substring(0, geospatialFileToIndex.getName().indexOf("."));

            // Grab the relative path
            String relativePath = absolutePathLocal.substring(this.filePathBaseLocal.length());

            // Construct the path on the geoserver
            String geoserverPath = "file:" + this.geoserverFilePathBase + relativePath;

            // Create a store name from the file name
            String storeName = convertFileNameToStoreName(filename);

            // Let's see if it's a shapefile
            if (contentType.equals("application/x-shapefile") && fileExtension != null && fileExtension.equals("shp")) {


                // See if the file is already listed in the store
                HttpResponse<String> getResponse = null;
                try {
                    getResponse = Unirest.get(this.geoserverBaseUrl + "/rest/workspaces/" + repoName +
                            "/datastores/" + storeName + ".xml")
                            .header("accept", "application/json")
                            .basicAuth(this.geoserverUsername, this.geoserverPassword)
                            .asString();
                } catch (UnirestException e) {
                    logger.error("UnirestException caught trying to get list of workspaces: " + e.getMessage());
                }

                // If the response code is 200, it already exists, if it's 404, it's not there yet
                if (getResponse.getStatus() == 404) {

                    // Construct the URL to call
                    String postUrl = this.geoserverBaseUrl + "/rest/workspaces/" + repoName + "/datastores/" +
                            storeName + "/external.shp";

                    HttpResponse<String> response = null;
                    try {
                        response = Unirest.put(postUrl)
                                .header("content-type", "text/plain")
                                .basicAuth(this.geoserverUsername, this.geoserverPassword)
                                .body(geoserverPath)
                                .asString();
                    } catch (UnirestException e) {
                        logger.error("UnirestException caught trying to get create store " + storeName +
                                ": " + e.getMessage());
                    }
                }
            }

            // Now what about GeoTIFFs?
            if (contentType.equals("image/tiff") && fileExtension != null && fileExtension.equals("tif")) {
                // Try to open the file as a GeoTIFF
                ImageMetadata metadata = null;
                try {
                    metadata = Imaging.getMetadata(geospatialFileToIndex);
                } catch (ImageReadException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (metadata != null) {
                    List metadataItems = metadata.getItems();
                    if (metadataItems != null) {
                        for (Object metadataItem : metadataItems) {
                            if (metadataItem.toString().startsWith("Geo")) {
                                logger.debug("Looks like a GeoTIFF");
                                // See if the file is already listed in the store
                                HttpResponse<String> getResponse = null;
                                try {
                                    getResponse = Unirest.get(this.geoserverBaseUrl + "/rest/workspaces/" + repoName +
                                            "/coveragestores/" + storeName)
                                            .header("accept", "application/json")
                                            .basicAuth(this.geoserverUsername, this.geoserverPassword)
                                            .asString();
                                } catch (UnirestException e) {
                                    logger.error("UnirestException caught trying to get list of workspaces: " + e.getMessage());
                                }

                                // If the response code is 200, it already exists, if it's 404, it's not there yet
                                if (getResponse.getStatus() == 404) {

                                    // Construct the URL to call
                                    String postUrl = this.geoserverBaseUrl + "/rest/workspaces/" + repoName + "/coveragestores";

                                    HttpResponse<String> response = null;
                                    try {
                                        response = Unirest.post(postUrl)
                                                .header("content-type", "application/json")
                                                .basicAuth(this.geoserverUsername, this.geoserverPassword)
                                                .body("{\"coverageStore\":{" +
                                                        "\"name\":\"" + storeName + "\"," +
                                                        "\"url\":\"" + geoserverPath + "\"," +
                                                        "\"workspace\":\"" + repoName + "\"," +
                                                        "\"type\":\"GeoTIFF\"," +
                                                        "\"enabled\":\"true\"}}")
                                                .asString();
                                    } catch (UnirestException e) {
                                        logger.error("UnirestException caught trying to get create store " + storeName +
                                                ": " + e.getMessage());
                                    }
                                }

                                break;
                            } else {
                                logger.debug("Not a geo property");
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param workspaceName
     * @return
     */
    private boolean doesWorkspaceExist(String workspaceName) {
        // The boolean to return
        boolean workspaceExists = false;

        // Try to grab the list of workspace names
        HttpResponse<JsonNode> response = null;
        try {
            response = Unirest.get(this.geoserverBaseUrl + "/rest/workspaces")
                    .header("accept", "application/json")
                    .basicAuth(this.geoserverUsername, this.geoserverPassword)
                    .asJson();
        } catch (UnirestException e) {
            logger.error("UnirestException caught trying to get list of workspaces: " + e.getMessage());
        }

        // If the response is there, try to find the workspace name in the list returned
        if (response.getBody() != null) {
            // Grab the body object
            JSONObject bodyObject = response.getBody().getObject();
            if (bodyObject != null) {
                // Look for workspaces object
                if (bodyObject.keySet().contains("workspaces")) {
                    // Grab it
                    JSONObject workspacesObject = bodyObject.getJSONObject("workspaces");
                    if (workspacesObject != null) {
                        // Check for the workspace key
                        if (workspacesObject.keySet().contains("workspace")) {
                            // Now grab the array named "workspace"
                            JSONArray workspaceArray = workspacesObject.getJSONArray("workspace");
                            if (workspaceArray != null) {
                                for (Object workspaceObject : workspaceArray) {
                                    // Make sure it's a JSONObject
                                    if (workspaceObject instanceof JSONObject) {
                                        JSONObject workspaceJSONObject = (JSONObject) workspaceObject;
                                        // Grab the name
                                        String name = workspaceJSONObject.getString("name");
                                        if (name != null && name.equals(workspaceName)) workspaceExists = true;
                                    } else {
                                        logger.warn("Was expecting JSONObject from workspaceArray, but not so.");
                                    }
                                }
                            } else {
                                logger.warn("While trying to look up workspace " + workspaceName +
                                        " the individual workspace object was null");
                            }
                        } else {
                            logger.warn("While trying to look up workspace " + workspaceName +
                                    " the individual workspace key was not found");
                        }
                    } else {
                        logger.warn("While trying to look up workspace " + workspaceName +
                                " the workspaces object was null");
                    }
                } else {
                    logger.warn("While trying to look up workspace " + workspaceName +
                            " the body did not have a workspaces key");
                }
            } else {
                logger.warn("While trying to look up workspace " + workspaceName +
                        " the body of the response was empty");
            }
        } else {
            logger.warn("While trying to look up workspace " + workspaceName + " the JsonNode response was empty");
        }

        // Return the result
        return workspaceExists;
    }

    /**
     * @param workspaceName
     */
    private void createWorkspace(String workspaceName) {
        // Check to see if the workspace exists yet
        if (!doesWorkspaceExist(workspaceName)) {

            // This is bizarre, but the response to a JSON post to create a workspace is just the name of
            // the workspace echoed back to the
            HttpResponse<String> response = null;
            try {
                response = Unirest.post(this.geoserverBaseUrl + "/rest/workspaces")
                        .header("accept", "application/json")
                        .header("content-type", "application/json")
                        .basicAuth(this.geoserverUsername, this.geoserverPassword)
                        .body("{\"workspace\":{\"name\":\"" + workspaceName + "\"}}")
                        .asString();
            } catch (UnirestException e) {
                logger.error("UnirestException caught trying to get create workspaces " + workspaceName +
                        ": " + e.getMessage());
            }

        }
    }

    private String convertFileNameToStoreName(String filename) {
        // Replace any spaces with dashes
        String spacelessFilename = filename.trim().replaceAll(" ", "-");

        return spacelessFilename;
    }
}
