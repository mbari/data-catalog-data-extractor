package org.mbari.data_catalog.extractor;

import com.google.gson.internal.LinkedTreeMap;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NetCDFFileToTimescaleIndexer {

    // A logger
    private static Logger logger = LogManager.getLogger(NetCDFFileToTimescaleIndexer.class);


    // This is the data source that is used to work with the database
    private ComboPooledDataSource comboPooledDataSource;

    // The database connection
    private Connection dbConnection;

    /**
     * @param comboPooledDataSource
     */
    public NetCDFFileToTimescaleIndexer(ComboPooledDataSource comboPooledDataSource) {
        this.comboPooledDataSource = comboPooledDataSource;
        try {
            this.dbConnection = this.comboPooledDataSource.getConnection();
        } catch (SQLException e) {
            logger.error("SQLException caught trying to get DB connection: " + e.getMessage());
        }
    }

    /**
     * This method reads in a NetCDF file and indexes the time series data into the associated database
     *
     * @param fileToIndex
     * @param uuid
     * @param dataTreeMap
     * @param numberOfRecordsAlreadyProcessed
     */
    public Long indexFile(File fileToIndex, String uuid, LinkedTreeMap dataTreeMap,
                          Long numberOfRecordsAlreadyProcessed) {

        // Start a record counter
        long recordCounter = 0;

        // Make sure UUID is specified
        if (uuid != null) {

            // Let's make sure the incoming file is available locally
            if (fileToIndex != null && fileToIndex.exists()) {

                // This is flag to indicate if the file has a time index (coordinate)
                boolean timeDimensionExists = false;

                // This is the number of data points in the time coordinate
                Long timeDimensionLength = -1L;

                // Make sure the data tree map exists and it has a property with a key called dimensions
                if (dataTreeMap != null && dataTreeMap.containsKey("dimensions")) {

                    // Check to see if the dimensions are there and if they are an array list
                    if (dataTreeMap.get("dimensions") != null && dataTreeMap.get("dimensions") instanceof ArrayList) {

                        // Grab the dimensions
                        ArrayList<LinkedTreeMap> dimensions = null;
                        try {
                            dimensions = (ArrayList<LinkedTreeMap>) dataTreeMap.get("dimensions");
                        } catch (Exception e) {
                            logger.error("Exception caught trying to cast dimension into an " +
                                    "ArrayList of LinkedTreeMaps: " + e.getMessage());
                        }

                        // Make sure it exists and there are dimensions
                        if (dimensions != null && dimensions.size() > 0) {

                            // Loop over the dimensions
                            for (LinkedTreeMap<String, String> dimension : dimensions) {

                                // If the short name is time, set the flag and grab the length if available
                                if (dimension.containsKey("short_name") &&
                                        dimension.get("short_name") != null &&
                                        dimension.get("short_name").toLowerCase().equals("time")) {

                                    // Set the flag that we do have a time dimension
                                    timeDimensionExists = true;

                                    // Look to see if the dimension also contains a property called length
                                    if (dimension.containsKey("length") && dimension.get("length") != null) {
                                        try {
                                            timeDimensionLength = Long.parseLong(dimension.get("length"));
                                        } catch (NumberFormatException e) {
                                            logger.warn("NumberFormatException caught trying to convert the length of " +
                                                    dimension.get("length") + " associated with the time dimension: " +
                                                    e.getMessage());
                                        }
                                    }
                                }
                            }
                        } else {
                            logger.warn(fileToIndex.getAbsolutePath() +
                                    " had a dimensions key, but no dimensions in the value");
                        }
                    } else {
                        logger.warn("The metadata for file " + fileToIndex.getAbsolutePath() +
                                " has a property named 'dimensions' but it is either null or not an array list");
                    }
                } else {
                    logger.warn("No dimensions found in NetCDF file " + fileToIndex.getAbsolutePath() +
                            " so no data was indexed into the database");
                }
                logger.debug("Is there a time dimension? " + timeDimensionExists);
                logger.debug("Length of time dimension is " + timeDimensionLength);

                // We need the time dimension to index the file, so make sure we have it
                if (timeDimensionExists) {

                    // Check to see if the table for the given file exists, and if not, create it
                    if (!doesTableExist(uuid)) {
                        this.createTable(uuid, dataTreeMap);
                    }

                    // Check for the table again to make sure it was created OK
                    if (doesTableExist(uuid)) {

                        logger.debug("OK, table should be ready for some data!");

                        // Let's now open the file for reading
                        NetcdfFile ncfile = null;
                        try {
                            ncfile = NetcdfFile.open(fileToIndex.getAbsolutePath());
                        } catch (IOException e) {
                            logger.error("IOException caught trying to open " + fileToIndex.getAbsolutePath() +
                                    ": " + e.getMessage());
                        }

                        // Make sure the file handle exists
                        if (ncfile != null) {

                            // Try to grab the time and space variables
                            Variable timeVariable = ncfile.findVariable("time");
                            Variable latitudeVariable = ncfile.findVariable("latitude");
                            Variable longitudeVariable = ncfile.findVariable("longitude");
                            Variable depthVariable = ncfile.findVariable("depth");
                            Variable altitudeVariable = ncfile.findVariable("altitude");

                            // This is the size of the array we are requesting, which is just one record by default
                            int[] size = new int[]{1};

                            // Make sure we have time variable first
                            if (timeVariable != null) {

                                // This is the size of the batches to insert
                                int batchSizeLimit = 1000;

                                // This is the counter tracking how many inserts have been added to the batch
                                int numInsertsAdded = 0;

                                // This is the batch statement
                                Statement batchStatement = null;

                                // An arraylist used to filter out duplicate timestamps
                                ArrayList<Long> processedTimestamps = new ArrayList<>();

                                // Iterate over the length of the time coordinate
                                for (int i = 0; i < timeDimensionLength; i++) {

                                    // Bump the record counter
                                    recordCounter++;

                                    // Create the query array which should be the same index of the time value
                                    int[] origin = new int[]{i};

                                    // Try to read in the timestamp
                                    Long timestamp = null;
                                    try {
                                        Array timeArray = timeVariable.read(origin, size);
                                        timestamp = (long) timeArray.getDouble(0);
                                    } catch (IOException e) {
                                        logger.error("IOException caught trying to read time at index " + i +
                                                ": " + e.getMessage());
                                    } catch (InvalidRangeException e) {
                                        logger.error("InvalidRangeException caught trying to read " +
                                                "time at index " + i + ": " + e.getMessage());
                                    }

                                    // Make sure we haven't processed this already
                                    if (recordCounter > numberOfRecordsAlreadyProcessed) {

                                        // Check to see if the timestamp was already processed
                                        if (!processedTimestamps.contains(timestamp)) {
                                            // Create the columnBuilder
                                            StringBuilder columnBuilder = new StringBuilder();

                                            // Add the insert clause
                                            columnBuilder.append("INSERT INTO \"" + uuid + "\" (");

                                            // And the value builder
                                            StringBuilder valueBuilder = new StringBuilder();
                                            valueBuilder.append("(");

                                            // Placeholders for time and space variables
                                            Double latitude = null;
                                            Double longitude = null;
                                            Double depth = null;
                                            Double altitude = null;

                                            // Let's see if we have a timestamp
                                            if (timestamp != null) {

                                                // Add the column and value for the timestamp
                                                columnBuilder.append("data_catalog_timestamp");
                                                valueBuilder.append("TO_TIMESTAMP(" + timestamp + ")");

                                                // Now try to read in the latitude if the variable was found
                                                if (latitudeVariable != null) {
                                                    try {
                                                        Array latitudeArray = latitudeVariable.read(origin, size);
                                                        latitude = latitudeArray.getDouble(0);
                                                    } catch (IOException e) {
                                                        logger.error("IOException caught trying to read data at index " +
                                                                i + " from the latitude variable which was found: " +
                                                                e.getMessage());
                                                    } catch (InvalidRangeException e) {
                                                        logger.error("InvalidRangeException caught trying to read data at " +
                                                                "index " + i + " from the latitude variable which was found: " +
                                                                e.getMessage());
                                                    }
                                                }


                                                // Now try the longitude if it was found
                                                if (longitudeVariable != null) {

                                                    try {
                                                        Array longitudeArray = longitudeVariable.read(origin, size);
                                                        longitude = longitudeArray.getDouble(0);
                                                    } catch (IOException e) {
                                                        logger.error("IOException caught trying to read data at index " +
                                                                i + " from the longitude variable which was found: " +
                                                                e.getMessage());
                                                    } catch (InvalidRangeException e) {
                                                        logger.error("InvalidRangeException caught trying to read data at " +
                                                                "index " + i + " from the longitude variable which was found: " +
                                                                e.getMessage());
                                                    }
                                                }

                                                // Now, try to read from depth if that variable was found
                                                if (depthVariable != null) {
                                                    try {
                                                        Array depthArray = depthVariable.read(origin, size);
                                                        depth = depthArray.getDouble(0);
                                                    } catch (IOException e) {
                                                        logger.error("IOException caught trying to read data at index " +
                                                                i + " from the depth variable which was found: " +
                                                                e.getMessage());
                                                    } catch (InvalidRangeException e) {
                                                        logger.error("InvalidRangeException caught trying to read data at " +
                                                                "index " + i + " from the depth variable which was found: " +
                                                                e.getMessage());
                                                    }
                                                }

                                                // And lastly, try to read altitude if it's there
                                                if (altitudeVariable != null) {
                                                    try {
                                                        Array altitudeArray = altitudeVariable.read(origin, size);
                                                        altitude = altitudeArray.getDouble(0);
                                                    } catch (IOException e) {
                                                        logger.error("IOException caught trying to read data at index " +
                                                                i + " from the altitude variable which was found: " +
                                                                e.getMessage());
                                                    } catch (InvalidRangeException e) {
                                                        logger.error("InvalidRangeException caught trying to read data at " +
                                                                "index " + i + " from the altitude variable which was found: " +
                                                                e.getMessage());
                                                    }
                                                }


                                                // Add the column name for the location
                                                columnBuilder.append(", data_catalog_location");

                                                // If latitude and longitude were found, create a geometry and add
                                                if (latitude != null && longitude != null) {
                                                    // TODO kgomes - should I check for valid range here?
                                                    valueBuilder.append(", ST_SetSRID(ST_MakePoint(" + longitude + ", " +
                                                            latitude + "), 4326)");
                                                } else {
                                                    // Since lat and lon were not found, insert a null
                                                    valueBuilder.append(", null");
                                                }

                                                // TODO kgomes - figure out what to do with depth and altitude

                                                // Now iterate all the variables
                                                List<Variable> variables = ncfile.getVariables();
                                                for (Variable variable : variables) {

                                                    // Grab the data type
                                                    DataType variableDataType = variable.getDataType();

                                                    // Grab the variables short name
                                                    String variableShortName = variable.getShortName();

                                                    // Grab the equivalent column name
                                                    String variableColumnName =
                                                            this.convertVariableNameToColumnName(variableShortName);

                                                    // Examine the dimensions to make sure time is one of them
                                                    boolean isTimeADimension = false;
                                                    List<Dimension> dimensions = variable.getDimensions();
                                                    if (dimensions != null) {
                                                        for (Dimension dimension : dimensions) {
                                                            if (dimension.getShortName() != null &&
                                                                    dimension.getShortName().equals("time")) {
                                                                isTimeADimension = true;
                                                            }
                                                        }
                                                    }

                                                    // If time is listed as a dimension, process the variable
                                                    if (isTimeADimension) {

                                                        try {
                                                            // Read the value
                                                            Array variableArray = variable.read(origin, size);

                                                            // Check to see if the variable is the timestamp
                                                            if (variableShortName.equals("time")) {
                                                                // Add the column name
                                                                columnBuilder.append(", time");

                                                                // Grab the value which should be double
                                                                Double timeValue = variableArray.getDouble(0);

                                                                // Make sure it's actually a number
                                                                if (timeValue == null || timeValue.isNaN()) {
                                                                    // Add a null value
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    // Cast the double to a long and add it as a timestamp
                                                                    valueBuilder.append(", TO_TIMESTAMP(" +
                                                                            (long) timeValue.doubleValue() + ")");
                                                                }
                                                            } else if (variableDataType == DataType.BOOLEAN) {
                                                                // Write the column name
                                                                columnBuilder.append(", " + variableColumnName);

                                                                // Try to read the value as a boolean
                                                                Boolean variableValue = variableArray.getBoolean(0);

                                                                // Make sure it looks OK, otherwise, write a null
                                                                if (variableValue == null) {
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    valueBuilder.append(", " + variableValue);
                                                                }
                                                            } else if (variableDataType == DataType.DOUBLE) {
                                                                // Write the column header
                                                                columnBuilder.append(", " + variableColumnName);

                                                                // Try to read in the data value
                                                                Double variableValue = variableArray.getDouble(0);

                                                                // Make sure it's OK or write null
                                                                if (variableValue == null || variableValue.isNaN()) {
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    valueBuilder.append(", " + variableValue);
                                                                }
                                                            } else if (variableDataType == DataType.FLOAT) {
                                                                // Write the column name
                                                                columnBuilder.append(", " + variableColumnName);

                                                                // Read in the data value
                                                                Float variableValue = variableArray.getFloat(0);

                                                                // Make sure the data looks OK or just write null
                                                                if (variableValue == null || variableValue.isNaN()) {
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    valueBuilder.append(", " + variableValue);
                                                                }
                                                            } else if (variableDataType == DataType.INT) {
                                                                // Write the column header
                                                                columnBuilder.append(", " + variableColumnName);

                                                                // Read in the data value
                                                                Integer variableValue = variableArray.getInt(0);

                                                                // Make sure it's OK or write null
                                                                if (variableValue == null) {
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    valueBuilder.append(", " + variableValue);
                                                                }
                                                            } else if (variableDataType == DataType.LONG) {
                                                                // Write the column name
                                                                columnBuilder.append(", " + variableColumnName);

                                                                // Read in the value
                                                                Long variableValue = variableArray.getLong(0);

                                                                // Make sure it's OK or write null
                                                                if (variableValue == null) {
                                                                    valueBuilder.append(", null");
                                                                } else {
                                                                    valueBuilder.append(", " + variableValue);
                                                                }
                                                            } else {
                                                                logger.debug("The data type of variable " +
                                                                        variableColumnName + " is " + variableDataType +
                                                                        " which is not recogized as data I can use, " +
                                                                        "skipping it.");
                                                            }
                                                        } catch (IOException e) {
                                                            logger.error("IOException caught trying to read data " +
                                                                    "for variable " + variableShortName + ": " +
                                                                    e.getMessage());
                                                        } catch (InvalidRangeException e) {
                                                            logger.error("InvalidRangeException caught trying to read " +
                                                                    "data for variable " + variableShortName + ": " +
                                                                    e.getMessage());
                                                        }
                                                    }
                                                }

                                                // Close the column builder
                                                columnBuilder.append(") VALUES ");

                                                // And the value builder
                                                valueBuilder.append(")");

                                                // Stitch it all together
                                                String insertStatementString = columnBuilder.toString() +
                                                        valueBuilder.toString();

                                                // Make sure we have a SQL statement to add inserts to
                                                if (batchStatement == null) {
                                                    try {
                                                        batchStatement = this.dbConnection.createStatement();
                                                        logDBConnectionStats("After creating batch insert statement");
                                                    } catch (SQLException e) {
                                                        logger.error("SQLException caught trying to create batch " +
                                                                "insert statement: " + e.getMessage());
                                                    }
                                                }

                                                // Make sure there is batch statement
                                                if (batchStatement != null) {
                                                    try {
                                                        batchStatement.addBatch(insertStatementString);
                                                        numInsertsAdded++;
                                                    } catch (SQLException e) {
                                                        logger.error("SQLException caught trying to add statement " +
                                                                "to batch: " + e.getMessage());
                                                        logger.error(insertStatementString);
                                                    }

                                                    // If the number of statements has reached it's limit, do the batch
                                                    if (numInsertsAdded >= batchSizeLimit) {
                                                        try {
                                                            batchStatement.executeBatch();
                                                            logger.debug("Inserted " + numInsertsAdded + " records");
                                                        } catch (SQLException e) {
                                                            logger.error("SQLException caught trying to execute batch: " + e.getMessage());
                                                        }
                                                        // Close the batch insert statement
                                                        if (batchStatement != null) {
                                                            try {
                                                                batchStatement.close();
                                                            } catch (SQLException e) {
                                                                logger.error("SQLException caught trying to close " +
                                                                        "batch after execution: " + e.getMessage());
                                                            }
                                                            batchStatement = null;
                                                        }
                                                        logDBConnectionStats("After batch execute and batch close");
                                                        // Reset the counter
                                                        numInsertsAdded = 0;
                                                    }
                                                }
                                            } else {
                                                logger.warn("No timestamp was found at record " + i +
                                                        " so record skipped");
                                            }
                                        } else {
                                            logger.warn("Looks like the record " + recordCounter +
                                                    " with timestamp " + timestamp + " was already processed");
                                        }
                                    }

                                    // If the timestamp is not in the tracking array list add it
                                    if (!processedTimestamps.contains(timestamp)) processedTimestamps.add(timestamp);
                                }
                                // We are done looping over the records, but it's likely that we have some inserts
                                // that were added to the batch, but not executed
                                if (numInsertsAdded > 0 && batchStatement != null) {
                                    try {
                                        batchStatement.executeBatch();
                                        logger.debug("Inserted " + numInsertsAdded + " records");
                                    } catch (SQLException e) {
                                        logger.error("SQLException caught trying to run last batch execute: " +
                                                e.getMessage());
                                    }
                                    if (batchStatement != null) {
                                        try {
                                            batchStatement.close();
                                        } catch (SQLException e) {
                                            logger.error("SQLException caught after last batch statement close: " + e.getMessage());
                                        }
                                    }
                                    logDBConnectionStats("After final execute and close of batch");
                                }

                            } else {
                                logger.warn("There should be a time variable, but it came back null");
                            }
                        } else {
                            logger.warn("Tried to create the handle to the NetCDF file, but did not work.");
                        }

                        // If the file handle is there, close the file
                        if (ncfile != null) {
                            try {
                                ncfile.close();
                            } catch (IOException e) {
                                logger.error("IOException caught trying to close " + fileToIndex.getAbsolutePath() +
                                        ": " + e.getMessage());
                            }
                        }
                    } else {
                        logger.error("No table with name " + uuid + " exists and could not be created!");
                    }
                } else {
                    logger.warn("No time dimension exists for file " + fileToIndex.getAbsolutePath() +
                            " so no data was extracted");
                }
            } else {
                logger.warn("Incoming file to index " + fileToIndex.getAbsolutePath() +
                        " does not exist locally, no indexing will happen");
            }
        } else {
            logger.warn("No UUID was specified on the call to the indexFile function, nothing will happen");
        }

        // Now return the number of records processed
        return recordCounter;
    }

    /**
     * This method will return true if a table with the given name exists or false if the table does not exist
     *
     * @param uuid
     * @return
     */
    private boolean doesTableExist(String uuid) {

        // A flag indicating the table already exists
        boolean tableExists = false;

        // Make sure there is a table name to query for
        if (uuid != null && !uuid.equals("")) {
            // Grab the metadata from the database
            DatabaseMetaData meta = null;
            try {
                meta = this.dbConnection.getMetaData();
            } catch (SQLException e) {
                logger.error("SQLException trying to read metadata from database: " + e.getMessage());
            }

            // If it's found, keep going
            if (meta != null) {
                // Create a placeholder for the query results
                ResultSet resultSet = null;
                try {
                    // Run the query looking for the table
                    resultSet = meta.getTables(null, null, uuid, new String[]{"TABLE"});
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to find a table named " + uuid + ": " + e.getMessage());
                }

                // Make sure we have a result
                if (resultSet != null) {
                    try {
                        // See if there is an entry (there should only be one)
                        if (resultSet.next()) {
                            // If we are here, then the table exists
                            tableExists = true;
                        }
                    } catch (SQLException e) {
                        logger.error("SQLException looping over results that were returned from the query " +
                                "to find the tracking table");
                    }
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        logger.error("SQLException caught after trying to close result set");
                    }
                } else {
                    logger.warn("ResultSet came back null from query for table " + uuid);
                }
            } else {
                logger.warn("While checking to see if the table " + uuid +
                        " already exists, the Metadata from database came back null");
            }
        }

        logDBConnectionStats("After checking if table " + uuid + " exists");

        // Return the result
        return tableExists;
    }

    /**
     * @param uuid
     * @param dataMap
     */
    private void createTable(String uuid, LinkedTreeMap dataMap) {

        logger.debug("createTable called for uuid " + uuid);

        // Make sure the data map exists
        if (dataMap != null) {

            // The table structures all start the same with a timestamp column and a location (geometry) column
            StringBuilder createTableBuilder = new StringBuilder();

            createTableBuilder.append("CREATE TABLE \"" + uuid + "\" (");
            createTableBuilder.append("data_catalog_timestamp timestamp PRIMARY KEY,");
            createTableBuilder.append("data_catalog_location geometry");

            // Check to see if there are variables listed
            if (dataMap.containsKey("variables")) {
                // Grab the TreeMap
                ArrayList<LinkedTreeMap> variables = (ArrayList<LinkedTreeMap>) dataMap.get("variables");

                // Make sure something came back
                if (variables != null) {

                    // Loop over all the variables
                    for (LinkedTreeMap variable : variables) {

                        // Make sure there is a short name and there are dimension short names associated with it
                        if (variable.containsKey("short_name") && variable.get("short_name") != null &&
                                variable.containsKey("dimension_short_names")) {

                            // Grab the variable short name
                            String variableShortName = (String) variable.get("short_name");

                            // Create a parallel table name
                            String variableColumnName = this.convertVariableNameToColumnName(variableShortName);

                            // Make sure the dimension short names are in an array list
                            if (variable.get("dimension_short_names") != null &&
                                    variable.get("dimension_short_names") instanceof ArrayList) {

                                // Cast it
                                ArrayList<String> dimensionShortNames =
                                        (ArrayList<String>) variable.get("dimension_short_names");

                                // Now loop through dimension names
                                for (String dimensionShortName : dimensionShortNames) {

                                    // See if the name is 'time' which would mean we can index the variable by time
                                    if (dimensionShortName.equals("time")) {

                                        // Make sure we have a data type
                                        if (variable.containsKey("data_type") && variable.get("data_type") != null &&
                                                variable.get("data_type") instanceof String) {

                                            // Grab the data type
                                            String dataType = (String) variable.get("data_type");
                                            boolean dataTypeValid = false;

                                            // Start the column clause
                                            String columnClause = ", " + variableColumnName + " ";

                                            // Make sure it's a data type that we can insert into the database
                                            if (variableShortName.equals("time")) {
                                                dataTypeValid = true;
                                                columnClause += "timestamp";
                                            } else if (dataType.equals(DataType.BOOLEAN.toString())) {
                                                dataTypeValid = true;
                                                columnClause += "boolean";
                                            } else if (dataType.equals(DataType.DOUBLE.toString())) {
                                                dataTypeValid = true;
                                                columnClause += "float8";
                                            } else if (dataType.equals(DataType.FLOAT.toString())) {
                                                dataTypeValid = true;
                                                columnClause += "float8";
                                            } else if (dataType.equals(DataType.INT.toString())) {
                                                dataTypeValid = true;
                                                columnClause += "integer";
                                            } else if (dataType.equals(DataType.LONG.toString())) {
                                                dataTypeValid = true;
                                                columnClause += "bigint";
                                            }

                                            // Add the variable name
                                            if (dataTypeValid) {
                                                createTableBuilder.append(columnClause);
                                            }
                                        }

                                        // Since we found the time coordinate, break out of loop
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
            }

            // Finish the create statement
            createTableBuilder.append(")");
            logger.debug("Create table statement: " + createTableBuilder.toString());

            // Go ahead and create the table
            Statement createTableStatement = null;
            try {
                createTableStatement = this.dbConnection.createStatement();
                if (createTableStatement != null) {
                    createTableStatement.executeUpdate(createTableBuilder.toString());
                }
            } catch (SQLException e) {
                logger.error("SQLException caught trying to create statement to create table for " + uuid + ": " +
                        e.getMessage());
            }
            if (createTableStatement != null) {
                try {
                    createTableStatement.close();
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to close create table statement for table " +
                            uuid + ": " + e.getMessage());
                }
            }
            logDBConnectionStats("After creating table " + uuid);
        } else {
            logger.warn("No dataMap was provided so no table was created");
        }
    }

    /**
     * This method takes in a String that is variable name and then converts it to something that can be used for a
     * column name in a database.  Some variable names have spaces and special characters which do not work as names.
     * This method basically replaces any non-usable characters with underscores.
     *
     * @param variableName
     * @return
     */
    private String convertVariableNameToColumnName(String variableName) {

        // The string to return
        String columnName = null;

        // If there is a variable name
        if (variableName != null) {
            // Replace any non alpha-numerics with underscores
            columnName = variableName.replaceAll("[^A-Za-z0-9]", "_");

            // Now replace duplicate underscores with just one
            columnName = columnName.replaceAll("_{2,}", "_");

            // Remove any starting or ending underscores
            columnName = columnName.replaceAll("_$", "");
            columnName = columnName.replaceAll("^_", "");

            // Now all should be lower case
            columnName = columnName.toLowerCase();

            // TODO kgomes - there is a limit to how long column names can be, I should enforce that
        }

        // Return the name
        return columnName;
    }

    /**
     * @param context
     */
    private void logDBConnectionStats(String context) {
//        try {
//            logger.debug("Context: " + context);
//            logger.debug("DB: NumConnections: " + this.comboPooledDataSource.getNumConnections());
//            logger.debug("DB: NumBusyConnections: " + this.comboPooledDataSource.getNumBusyConnections());
//        } catch (SQLException e) {
//            logger.error("SQLException caught trying to log DB connection stats: " + e.getMessage());
//        }
    }

}
