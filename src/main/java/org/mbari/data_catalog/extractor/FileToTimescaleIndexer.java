package org.mbari.data_catalog.extractor;

import com.google.gson.internal.LinkedTreeMap;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;

/**
 * This class is responsible for taking in a reference to a file and indexing the data in the file (if possible) into
 * a Timescale database.
 */
public class FileToTimescaleIndexer {

    // A logger
    private static Logger logger = LogManager.getLogger(FileToTimescaleIndexer.class);

    // This is the data source that is used to work with the database
    private ComboPooledDataSource comboPooledDataSource;

    // The database connection
    private Connection dbConnection;

    // The Tika extraction client
    private Tika tika = new Tika();

    // The CSV indexer
    private CSVFileToTimescaleIndexer csvFileToTimescaleIndexer = null;

    // The NetCDF indexer
    private NetCDFFileToTimescaleIndexer netCDFFileToTimescaleIndexer = null;

    /**
     * This is the default constructor
     */
    public FileToTimescaleIndexer(String databaseHost, Integer databasePort, String databaseName,
                                  String databaseUsername, String databasePassword) throws IllegalArgumentException {

        // Build the URL to connect to the DB
        StringBuilder dbUrlBuilder = new StringBuilder();
        dbUrlBuilder.append("jdbc:postgresql://");

        // Make sure we have a hostname
        if (databaseHost != null) {
            dbUrlBuilder.append(databaseHost);
        } else {
            throw new IllegalArgumentException("No database hostname was specified");
        }

        // If a port was specified, add that (if not, hope the default port of 5432 is the one they want
        if (databasePort != null) {
            dbUrlBuilder.append(":" + databasePort);
        }

        // Check for database name
        if (databaseName != null) {
            dbUrlBuilder.append("/" + databaseName);
        } else {
            throw new IllegalArgumentException("Database name was not specified");
        }

        // Create the pooled data source
        comboPooledDataSource = new ComboPooledDataSource();

        try {
            comboPooledDataSource
                    .setDriverClass("org.postgresql.Driver");
        } catch (PropertyVetoException e) {
            throw new IllegalArgumentException("Something went wrong trying to load the Postgres driver: " +
                    e.getMessage());
        }

        comboPooledDataSource
                .setJdbcUrl(dbUrlBuilder.toString());
        // If the username was specified
        if (databaseUsername != null) {
            comboPooledDataSource.setUser(databaseUsername);
            if (databasePassword != null) {
                comboPooledDataSource.setPassword(databasePassword);
            }
        }

        // Create a connection to continue to use
        try {
            this.dbConnection = this.comboPooledDataSource.getConnection();
        } catch (SQLException e) {
            logger.error("SQLException caught trying to get connection: " + e.getMessage());
        }

        // Create the indexing classes
        this.csvFileToTimescaleIndexer = new CSVFileToTimescaleIndexer(this.comboPooledDataSource);
        this.netCDFFileToTimescaleIndexer = new NetCDFFileToTimescaleIndexer(this.comboPooledDataSource);

        // Now make sure there is a table to track indexing of data files
        this.createIndexTrackingTable();
    }

    /**
     * @param fileToIndex
     * @param uuid
     * @param dataTreeMap
     */
    public void indexFile(File fileToIndex, String uuid, LinkedTreeMap dataTreeMap) {
        logger.debug("Will try to index file " + fileToIndex.getAbsolutePath());

        // Make sure UUID is provided
        if (uuid != null && !uuid.equals("")) {

            // Let's make sure we even have access to this file.
            if (fileToIndex.exists()) {

                // Now grab the size of the file and last modified timestamp
                long localFileSize = fileToIndex.length();
                long localLastModifiedEpochSeconds = fileToIndex.lastModified();

                // Create a local variable to hold how many records from this file have already been processed
                long numRecordsProcessed = 0L;

                // Grab the tracking stats for the UUID from the tracking table
                HashMap<String, Long> indexTrackingStats = this.getIndexTrackingStats(uuid);

                // A boolean to indicate that the file needs indexing and set it to true by default
                boolean fileNeedsIndexing = true;

                // If there is a file size and last modified timestamp in the stats, check to see if they are the same
                // as the local ones, that would mean the file is already indexed fully
                if (indexTrackingStats != null) {
                    // Check to see if file size and last mod timestamp are the same
                    if (indexTrackingStats.containsKey("file_size") &&
                            indexTrackingStats.get("file_size") != null &&
                            indexTrackingStats.get("file_size") == localFileSize &&
                            indexTrackingStats.containsKey("last_modified_epoch_seconds") &&
                            indexTrackingStats.get("last_modified_epoch_seconds") != null &&
                            indexTrackingStats.get("last_modified_epoch_seconds") == localLastModifiedEpochSeconds) {
                        fileNeedsIndexing = false;
                    }

                    // Grab the number of records processed
                    if (indexTrackingStats.containsKey("num_records_processed") &&
                            indexTrackingStats.get("num_records_processed") != null) {
                        numRecordsProcessed = indexTrackingStats.get("num_records_processed");
                    }
                }

                // So let's see if we need to index the file or not
                if (fileNeedsIndexing) {

                    // OK, so now, let's start the indexing.  The first thing we need to do is figure out what kind
                    // of file it is
                    String mimeType = null;
                    try {
                        // TODO kgomes - I should probably take this from the file information from the catalog????
                        mimeType = tika.detect(fileToIndex);
                    } catch (IOException e) {
                        logger.error("IOException trying to detect mime type from file " +
                                fileToIndex.getAbsolutePath());
                        logger.error(e.getMessage());
                    }

                    // Make sure we have a MIME Type
                    if (mimeType != null) {

                        // Here is the new number of records that have been processed
                        Long newNumberOfRecordsProcessed = numRecordsProcessed;

                        // If it's a CSV file
                        if (mimeType.equals("text/csv")) {
                            // Index it using the CSV indexer
                            newNumberOfRecordsProcessed =
                                    csvFileToTimescaleIndexer.indexFile(fileToIndex, uuid, dataTreeMap, numRecordsProcessed);
                        } else if (mimeType.equals("application/x-hdf")) {
                            newNumberOfRecordsProcessed =
                                    netCDFFileToTimescaleIndexer.indexFile(fileToIndex, uuid, dataTreeMap, numRecordsProcessed);
                        } else {
                            logger.warn("The file " + fileToIndex.getAbsolutePath() + " should have data associated " +
                                    "with it, but it does not have a MIMEType (" + mimeType + ") that I know how to parse");
                        }

                        // Now insert or update the tracking stats
                        this.persistIndexTrackingInfo(uuid, fileToIndex.getAbsolutePath(), localFileSize, localLastModifiedEpochSeconds,
                                newNumberOfRecordsProcessed, indexTrackingStats != null);
                    }
                } else {
                    logger.debug("Seems like the file does not need indexing");
                }
            } else {
                logger.warn("I was asked to index the file " + fileToIndex.getAbsolutePath() + " but I don't seem " +
                        "to have access to it locally (or it doesn't exist) so I can't do very much.");
            }
        } else {
            logger.warn("No UUID was provided, cannot do much without it.");
        }
    }

    /**
     * This method looks at the database connection and checks to see if there is a table in the database that is used
     * to track the indexing of data files.  If not, it will create one.
     */
    private void createIndexTrackingTable() {

        // First make sure the database connection is there and OK
        if (this.dbConnection != null) {

            // A flag indicating the table already exists
            boolean trackingTableExists = false;

            // Make sure we have a connection
            if (this.dbConnection != null) {

                // OK, things should be good to go, so let's grab the metadata from the database
                DatabaseMetaData meta = null;
                try {
                    meta = this.dbConnection.getMetaData();
                    logDBConnectionStats("createIndexTrackingTable->getMetaData");
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to get metadata from connection: " + e.getMessage());
                }

                // Make sure we got metadata
                if (meta != null) {

                    // Query for the tables
                    ResultSet res = null;
                    try {
                        res = meta.getTables(null, null, "indexing_tracker",
                                new String[]{"TABLE"});
                    } catch (SQLException e) {
                        logger.error("SQLException caught trying to look up table indexing_tracker: " +
                                e.getMessage());
                    }

                    // Make sure we got a result
                    if (res != null) {
                        // Loop over the results
                        try {
                            if (res.next()) {
                                // If we are here, then the table exists
                                trackingTableExists = true;
                            }
                        } catch (SQLException e) {
                            logger.error("SQLException caught trying to get next result from table query: " +
                                    e.getMessage());
                        }
                        // Close the result set
                        try {
                            res.close();
                        } catch (SQLException e) {
                            logger.error("SQLException caught trying to close result set: " + e.getMessage());
                        }
                    } else {
                        logger.warn("ResultSet came back null from query for tracking table.");
                    }
                } else {
                    logger.warn("Metadata from database came back null");
                }
            }
            logDBConnectionStats("createIndexTrackingTable: after checking if table already exists");

            // Now check to see if the table exists already
            if (!trackingTableExists) {

                // Keep the statement outside the try block so it can be closed if an exception happens
                Statement createTableStatement = null;

                try {
                    // Create a statement
                    createTableStatement = this.dbConnection.createStatement();
                    logDBConnectionStats("createIndexTrackingTable->createStatement: to insert " +
                            "the tracking table");
                } catch (SQLException e) {
                    logger.error("SQLException trying to get connection and statement from DB pool:" +
                            e.getMessage());
                }

                // Make sure we have a statement
                if (createTableStatement != null) {
                    try {
                        createTableStatement.executeUpdate("CREATE TABLE indexing_tracker " +
                                "(uuid text PRIMARY KEY, file_path text, file_size bigint," +
                                " last_modified_epoch_seconds bigint, num_records_processed bigint)");
                        logDBConnectionStats("createIndexTrackingTable: after executing create table");
                    } catch (SQLException e) {
                        logger.error("SQLException caught trying to create indexing table: " + e.getMessage());
                    }
                }

                // Close the statement
                if (createTableStatement != null) {
                    try {
                        createTableStatement.close();
                        logDBConnectionStats("createIndexTrackingTable: after statement close");
                    } catch (SQLException e) {
                        logger.error("SQLException caught trying to close the create table statement: " + e.getMessage());
                    }
                }
            } else {
                logger.debug("Tracking table exists, will not create one");
            }
        } else {
            logger.error("While trying to check for index tracking table, the database connection pool was null");
        }
    }


    /**
     * This method queries the index tracking table for the information related to the indexing of the file associated
     * with the given UUID.  It will return a HashMap that will have the keys "file_size", "last_modified_epoch_seconds",
     * and "num_records_processed" which point to Long values for those fields.  It's possible the values could be null
     * and if there is nothing for the given UUID, null is returned instead of a HashMap.
     *
     * @param uuid
     * @return
     */
    private HashMap<String, Long> getIndexTrackingStats(String uuid) {

        // The HashMap to return
        HashMap<String, Long> hashMapToReturn = null;

        // Make sure there is a UUID
        if (uuid != null && !uuid.equals("")) {

            // Create the query statement handle outside the try clause so it can be closed if exception is thrown
            Statement queryStatement = null;
            try {
                // Create the statement
                queryStatement = this.dbConnection.createStatement();
                logDBConnectionStats("getIndexTrackingStats->createStatement");

                // Run the query
                ResultSet rs = queryStatement.executeQuery("SELECT * from indexing_tracker WHERE uuid = '" +
                        uuid + "'");

                // Make sure there are results
                if (rs != null && rs.next()) {

                    // Retrieve by column name
                    Long fileSize = rs.getLong("file_size");
                    Long lastModifiedEpochSeconds = rs.getLong("last_modified_epoch_seconds");
                    Long numRecordsProcessed = rs.getLong("num_records_processed");

                    // Close up the results
                    rs.close();

                    // Create the HashMap
                    hashMapToReturn = new HashMap<>();

                    // Insert them into the hash map
                    hashMapToReturn.put("file_size", fileSize);
                    hashMapToReturn.put("last_modified_epoch_seconds", lastModifiedEpochSeconds);
                    hashMapToReturn.put("num_records_processed", numRecordsProcessed);

                }


            } catch (SQLException e) {
                logger.error("SQLException caught trying to read indexing tracking info for UUID " + uuid);
            }

            // Now close the query statement
            if (queryStatement != null) {
                try {
                    queryStatement.close();
                    logDBConnectionStats("getIndexTrackingStats: after statement close");
                } catch (SQLException e) {
                    logger.error("SQLException caught trying to close statement to get indexing stats: " +
                            e.getMessage());
                }
            }
        }
        logDBConnectionStats("After get index tracking stats");

        // Return the result
        return hashMapToReturn;
    }


    /**
     * @param uuid
     * @param fileSize
     * @param lastModifiedTimestamp
     * @param numRecordsProcessed
     * @param isUpdate
     */
    private void persistIndexTrackingInfo(String uuid, String filePath, Long fileSize, Long lastModifiedTimestamp,
                                          Long numRecordsProcessed, boolean isUpdate) {

        // Create the statement handle outside the try in so that it can be closed if the insert/update fails
        Statement persistStatement = null;
        try {
            // Create the SQL statement
            persistStatement = this.dbConnection.createStatement();
            logDBConnectionStats("persistIndexTrackingInfo->create statement");

            // Check to see if it's an update or insert
            if (isUpdate) {
                persistStatement.executeUpdate("UPDATE indexing_tracker (file_size, " +
                        "last_modified_epoch_seconds, num_records_processed) VALUES (" + fileSize +
                        "," + lastModifiedTimestamp + "," + numRecordsProcessed +
                        ") WHERE uuid = '" + uuid + "'");
            } else {
                persistStatement.execute("INSERT INTO indexing_tracker (uuid, file_path, file_size, " +
                        "last_modified_epoch_seconds, num_records_processed) VALUES ('" + uuid + "', '" + filePath +
                        "', " + fileSize + ", " + lastModifiedTimestamp + ", " +
                        numRecordsProcessed + ")");
            }

        } catch (SQLException e) {
            logger.error("SQLException caught trying to persist indexing stats for " + uuid);
        }

        // Close if we need to
        if (persistStatement != null) {
            try {
                persistStatement.close();
                logDBConnectionStats("persisteIndexTrackingInfo: closing statement after insert or update");
            } catch (SQLException e) {
                logger.error("SQLException caught trying to close statement used to insert/update tracking stats:" +
                        e.getMessage());
            }
        }
        logDBConnectionStats("After persist index tracking info");
    }

    /**
     * @param context
     */
    private void logDBConnectionStats(String context) {
//        try {
//            logger.debug("Context: " + context);
//            logger.debug("DB: NumConnections: " + this.comboPooledDataSource.getNumConnections());
//            logger.debug("DB: NumBusyConnections: " + this.comboPooledDataSource.getNumBusyConnections());
//        } catch (SQLException e) {
//            logger.error("SQLException caught trying to log DB connection stats: " + e.getMessage());
//        }
    }
}
